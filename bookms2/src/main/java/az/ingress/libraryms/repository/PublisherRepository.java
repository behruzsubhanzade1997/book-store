package az.ingress.libraryms.repository;

import az.ingress.libraryms.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher,Long> {
}
