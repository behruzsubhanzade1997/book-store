package az.ingress.libraryms.config.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;

@Slf4j
@Service
public class JwtService {
    private Key key;

    @Value("10000")
    private Long duration;

    @PostConstruct
    public void init(){
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("TGlicmFyeU1hbmFnZXJMaWJyYXJ5TWFuYWdlckxpYnJhcnlNYW5hZ2VyTGlicmFyeU1hbmFnZXJMaWJyYXJ5TWFuYWdlckxpYnJhcnlNYW5hZ2VyTGlicmFyeU1hbmFnZXI=");
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}
