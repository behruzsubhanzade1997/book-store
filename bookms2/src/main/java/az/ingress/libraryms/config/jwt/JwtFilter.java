package az.ingress.libraryms.config.jwt;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Configuration
@RequiredArgsConstructor

public class JwtFilter extends OncePerRequestFilter {

    private final static String AUTHORIZATION_HEADER = "Authorization";
    private final static String BEARER_HEADER = "Bearer";

    private final  JwtService jwtService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        response.setHeader("Access-Control-Allow-Methods","Get, POST, PUT, DELETE, OPTIONS");
        String authorization = request.getHeader(AUTHORIZATION_HEADER);
        if (authorization!=null){
            String tokenFromHeader = getTokenFromHeader(authorization);
            log.info("Authorization header {}",authorization);
            log.info("Token is {}",tokenFromHeader);
            Claims claims = jwtService.parseToken(tokenFromHeader);
            if (claims.getExpiration().before(new Date())){
                throw new RuntimeException("Token is expired");
            }
            Authentication authenticationBearer = getAuthenticationBearer(claims);
            SecurityContextHolder.getContext().setAuthentication(authenticationBearer);
            log.info("Claims from filter is {}",claims);
        }
        filterChain.doFilter(request,response);
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get("role",List.class);
        List<GrantedAuthority> authorityList = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(claims,"",authorityList);

    }

    private String getTokenFromHeader(String authorization) {
        if (authorization.startsWith(BEARER_HEADER)){
            return authorization.substring(BEARER_HEADER.length());
        }
        throw new RuntimeException("Bearertoken not found");
    }




}

