package az.ingress.libraryms.mapper;

import az.ingress.libraryms.dto.BookDto;
import az.ingress.libraryms.dto.PublisherDto;
import az.ingress.libraryms.dto.PublishingHouseDto;
import az.ingress.libraryms.model.Author;
import az.ingress.libraryms.model.Publisher;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.awt.print.Book;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MapperConfig {

    BookDto mapEntityToDto(Book book);
    Book mapDtoToEntity(BookDto bookDto);

    PublisherDto mapEntityToDto(Author author);
    Author mapDtoToEntity(PublisherDto publisherDto);

    PublishingHouseDto mapEntityToDto(Publisher publisher);
    Publisher mapDtoToEntity(PublishingHouseDto bookDto);

}
