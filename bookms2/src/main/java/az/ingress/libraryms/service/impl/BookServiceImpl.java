package az.ingress.libraryms.service.impl;

import az.ingress.libraryms.dto.BookDto;
import az.ingress.libraryms.model.Book;
import az.ingress.libraryms.repository.BookRepository;
import az.ingress.libraryms.service.BookService;
import az.ingress.libraryms.specifications.BookSpecification;
import az.ingress.libraryms.specifications.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
@Service
@RequiredArgsConstructor

public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;

    @Override
    public BookDto create(BookDto bookDto) {

        Book book = modelMapper.map(bookDto,Book.class);
//        Book save = bookRepository.save(book);
//        BookDto map = modelMapper.map(save, BookDto.class);
//        return map;
        return modelMapper.map(bookRepository.save(book),BookDto.class);
    }

    @Override
    public List<BookDto> getAll() {
        return bookRepository.findAll()
                .stream().map(entity -> modelMapper.map(entity, BookDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public BookDto getById(Long id) {
        Book praduct = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not id found"));
        BookDto praductDto = modelMapper.map(praduct, BookDto.class);
        return praductDto;

    }

    @Override
    public BookDto updateById(Long id, BookDto bookDto) {
        return null;
    }

    @Override
    public void deleteById(Long id) {
        bookRepository.deleteById(id);

    }

    @Override
    public List<BookDto> getAllByCriteria(final SearchCriteria searchCriteria, Pageable pageable) {
        final BookSpecification bookSpecification = new BookSpecification();
        bookSpecification.add(searchCriteria);
        return bookRepository.findAll(bookSpecification, (Sort) pageable).stream().map(book -> {
            return modelMapper.map(book, BookDto.class);
        }).collect(toList());

    }
}

