package az.ingress.libraryms.service;

import az.ingress.libraryms.dto.BookDto;
import az.ingress.libraryms.specifications.SearchCriteria;

import java.awt.print.Pageable;
import java.util.List;

public interface BookService {

    BookDto create  (BookDto bookDto);
    List<BookDto> getAll();

    BookDto getById(Long id);

    BookDto updateById(Long id, BookDto bookDto);

    void deleteById(Long id);

    List<BookDto> getAllByCriteria(SearchCriteria searchCriteria, Pageable pageable);
}
