package az.ingress.libraryms.controller;

import az.ingress.libraryms.dto.BookDto;
import az.ingress.libraryms.service.BookService;
import az.ingress.libraryms.specifications.SearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService service;


    @GetMapping
    public List<BookDto> getAll() {
        return service.getAll();
    } // As a user, I should be able to list books


    @GetMapping("/{id}")
    public BookDto getById(@PathVariable Long id) {
        return service.getById(id);
    } // As a user, I should be able to see the details of book and author (GET)

    @PutMapping("/{id}")
    public BookDto updateById(@Valid @PathVariable Long id,
                              @RequestBody BookDto bookDto) {
        return service.updateById(id, bookDto);
    } // As a publisher, I should be able update a specific book details that has been published by me

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        service.deleteById(id);
    } // delete book


    @PostMapping("/search")
    public List<BookDto> getAllByCriteria(@Valid @RequestBody SearchCriteria searchCriteria, Pageable pageable) {
        return service.getAllByCriteria(searchCriteria, pageable);

    } // As a user, I should be able to search specific book (pagination support)

}
