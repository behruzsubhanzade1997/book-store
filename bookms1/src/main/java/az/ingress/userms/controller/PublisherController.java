package az.ingress.userms.controller;


import az.ingress.userms.dto.RegistrationDto;
import az.ingress.userms.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/publisher")
public class PublisherController {

    private final UserServiceImpl userService;

    @PostMapping("/registration")
    public void register(@RequestBody RegistrationDto request) {
        userService.publisherRegister(request);
    }
}
