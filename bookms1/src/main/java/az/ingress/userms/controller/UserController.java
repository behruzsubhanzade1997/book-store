package az.ingress.userms.controller;

import az.ingress.userms.dto.LoginRequestDto;
import az.ingress.userms.dto.LoginResponseDto;
import az.ingress.userms.dto.RegistrationDto;
import az.ingress.userms.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/registration")
    public void register(@RequestBody RegistrationDto request) {
        userService.userRegister(request);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody @Valid LoginRequestDto request) {
        return ResponseEntity.ok(userService.login(request));
    }
}
