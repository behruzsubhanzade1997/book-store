package az.ingress.userms.model;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String email;
    String username;
    String password;
    String name;
    String surname;

    boolean isEnabled;
    boolean credentialNonExpired;
    boolean isAccountNonLocked;
    boolean isAccountNonExpired;
    boolean isCredentialsNonExpired;

    @Enumerated(EnumType.STRING)
    UserRole userRole;

   @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
   public List<Authority> authorities;

}