package az.ingress.userms.model;

public enum UserRole {

    ROLE_ADMIN,
    ROLE_PUBLISHER,
    ROLE_USER,

}
