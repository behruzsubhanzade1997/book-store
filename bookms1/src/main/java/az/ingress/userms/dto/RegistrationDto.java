package az.ingress.userms.dto;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class RegistrationDto {

    @NotBlank
    @Email
    String email;

    @NotBlank
    @Size(min = 6, max = 10)
    String password;

    @NotBlank
    @Size(min = 6, max = 16)
    String username;

    @NotBlank
    String name;


    @NotBlank
    String surname;

}
