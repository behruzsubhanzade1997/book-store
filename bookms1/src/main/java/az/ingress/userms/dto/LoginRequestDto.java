package az.ingress.userms.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginRequestDto {

    @NotBlank
    @Size(min = 6, max = 16)
    String username;

    @NotBlank
    @Size(min = 6, max = 12)
    String password;
}
