package az.ingress.userms.dto;

import az.ingress.userms.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtCredentials {

    String username;
    UserRole role;
    Long id;
}
