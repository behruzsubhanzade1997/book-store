package az.ingress.userms.service;

import az.ingress.userms.dto.JwtCredentials;

public interface SecurityService {

    JwtCredentials getCurrentJwtCredentials();
    Long getId();
}
