package az.ingress.userms.service.impl;

import az.ingress.userms.config.jwt.JwtService;
import az.ingress.userms.dto.JwtCredentials;
import az.ingress.userms.dto.LoginRequestDto;
import az.ingress.userms.dto.LoginResponseDto;
import az.ingress.userms.dto.RegistrationDto;
import az.ingress.userms.exception.UserNamePasswordValidException;
import az.ingress.userms.model.User;
import az.ingress.userms.repository.UserRepository;
import az.ingress.userms.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static az.ingress.userms.model.UserRole.ROLE_PUBLISHER;
import static az.ingress.userms.model.UserRole.ROLE_USER;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;


    @Override
    public UserDetails loadUserByUsername(String username) {
        log.info("Getting user information by user name {}", username);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " not found "));
        return user;
    }

    @Override
    public void userRegister(RegistrationDto request) {
        var user = userRepository.findByUsername(request.getUsername());
        if (user.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        log.info("Trying to create user");
        User user1 = User.builder()
                .username(request.getUsername())
                .password(bCryptPasswordEncoder.encode(request.getPassword()))
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .isEnabled(true)
                .userRole(ROLE_USER)
                .build();
        userRepository.save(user1);
    }


    public void publisherRegister(RegistrationDto request) {
        var user = userRepository.findByUsername(request.getUsername());
        if (user.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        log.info("Trying to create user");
        User user1 = User.builder()
                .username(request.getUsername())
                .password(bCryptPasswordEncoder.encode(request.getPassword()))
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .isEnabled(true)
                .userRole(ROLE_PUBLISHER)
                .build();
        userRepository.save(user1);
    }

    @Override
    public LoginResponseDto login(LoginRequestDto request) {
        var userDetails = (User) loadUserByUsername(request.getUsername());
        boolean matches = bCryptPasswordEncoder.matches(request.getPassword(), userDetails.getPassword());
        if (!matches) {
            throw new UserNamePasswordValidException();
        }
        return LoginResponseDto.builder()
                .jwt(jwtService.issueToken(JwtCredentials.builder()
                                .role(userDetails.getUserRole())
                                .username(userDetails.getUsername())
                                .build())).build();


    }
}