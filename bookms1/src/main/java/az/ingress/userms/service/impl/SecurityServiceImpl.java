package az.ingress.userms.service.impl;

import az.ingress.userms.dto.JwtCredentials;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor

public class SecurityServiceImpl {

    private final ModelMapper modelMapper = new ModelMapper();


    public JwtCredentials getCurrentJwtCredentials() {
        var securityContext = SecurityContextHolder.getContext();
        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> modelMapper.map(authentication.getPrincipal(), JwtCredentials.class))
                .orElseThrow(RuntimeException::new);
    }

    public Long getId() {
        return getCurrentJwtCredentials().getId();
    }
}
