package az.ingress.userms.service;

import az.ingress.userms.dto.LoginRequestDto;
import az.ingress.userms.dto.LoginResponseDto;
import az.ingress.userms.dto.RegistrationDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserDetails loadUserByUsername(String username);

    void userRegister(RegistrationDto request);

    LoginResponseDto login(LoginRequestDto request);
}
