package az.ingress.userms.config.jwt;

import az.ingress.userms.dto.JwtCredentials;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
public class JwtService {
    private Key key;

    @Value("10000")
    private Long duration;

    @PostConstruct
    public void init(){
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("TGlicmFyeU1hbmFnZXJMaWJyYXJ5TWFuYWdlckxpYnJhcnlNYW5hZ2VyTGlicmFyeU1hbmFnZXJMaWJyYXJ5TWFuYWdlckxpYnJhcnlNYW5hZ2VyTGlicmFyeU1hbmFnZXI=");
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken (JwtCredentials jwtCredentials){
        log.trace("Issue JWT token to {} for {} seconds",jwtCredentials,duration);
        final JwtBuilder jwtBuilder  = Jwts.builder()
                .setIssuedAt(new Date())
                .claim(JwtCredentials.Fields.id,jwtCredentials.getId())
                .claim(JwtCredentials.Fields.role,jwtCredentials.getRole())
                .claim(JwtCredentials.Fields.username,jwtCredentials.getUsername())
                .setExpiration(Date.from(Instant.now().plusSeconds(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }









}
