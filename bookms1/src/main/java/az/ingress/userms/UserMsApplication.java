package az.ingress.userms;

import az.ingress.userms.model.User;
import az.ingress.userms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static az.ingress.userms.model.UserRole.ROLE_USER;

@SpringBootApplication
@RequiredArgsConstructor
public class UserMsApplication implements CommandLineRunner {

	private final UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(UserMsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
